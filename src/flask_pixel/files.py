import os

from flask import Blueprint
from flask import current_app
from flask import render_template
from flask import request
from flask import send_from_directory
from sqlalchemy import select
from sqlalchemy.exc import NoResultFound
from werkzeug.exceptions import abort

from .database import db_session
from .models import File
from .models import Log
from .models import User

bp = Blueprint("files", __name__)


@bp.route("/")
def index():
    users = db_session.execute(select(User)).scalars().all()
    files = db_session.execute(select(File)).scalars().all()
    logs = db_session.execute(select(Log)).scalars().all()

    return render_template(
        "files/index.html", logs=logs, users=users, files=files
    )


@bp.route("/<user_hash>/<file_hash>")
def image(user_hash, file_hash):
    try:
        user = db_session.execute(
            select(User).filter_by(hash=user_hash)
        ).scalar_one()
        file = db_session.execute(
            select(File).filter_by(hash=file_hash)
        ).scalar_one()
    except NoResultFound:
        abort(403)

    log = Log(user=user, file=file, remote_addr=request.remote_addr)
    db_session.add(log)
    db_session.commit()

    return send_from_directory(
        directory=os.path.join(
            current_app.instance_path, current_app.config["FILES_ROOT"]
        ),
        path=file.filename,
        mimetype=file.mimetype,
    )
