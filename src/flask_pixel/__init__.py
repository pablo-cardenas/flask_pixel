"""Flask_pixel
"""

__version__ = "0.1"


import os
from flask import Flask
from jinja2 import StrictUndefined


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        DATABASE_URI="sqlite:///" + os.path.join(app.instance_path, "data.db"),
        SECRET_KEY="dev",
        FILES_ROOT="files",
    )
    app.jinja_env.undefined = StrictUndefined

    if test_config is None:
        app.config.from_pyfile("config.py", silent=True)
    else:
        app.config.from_mapping(test_config)

    os.makedirs(app.instance_path, exist_ok=True)
    files_root = os.path.join(app.instance_path, app.config["FILES_ROOT"])
    os.makedirs(files_root, exist_ok=True)

    @app.route("/hello")
    def hello():
        return "Hello, world!"

    from . import database

    database.init_app(app)

    from . import files

    app.register_blueprint(files.bp)

    from . import scripts

    scripts.init_app(app)
    return app
