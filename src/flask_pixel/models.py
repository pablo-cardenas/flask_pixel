from datetime import datetime
from hashlib import sha1

from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import ForeignKey
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy.orm import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()


def hash_column(column_name):
    def default_function(context):
        return sha1(
            context.current_parameters.get(column_name).encode()
        ).hexdigest()

    return default_function


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True)
    email: str = Column(String(200), nullable=False, unique=True)
    hash: str = Column(
        String(200),
        default=hash_column("email"),
        unique=True,
    )

    logs: list["Log"] = relationship("Log", back_populates="user")


class File(Base):
    __tablename__ = "file"

    id = Column(Integer, primary_key=True)
    filename: str = Column(String(200), nullable=False, unique=True)
    mimetype: str = Column(String(200), nullable=False)
    hash: str = Column(
        String(200),
        default=hash_column("filename"),
        unique=True,
    )

    logs: list["Log"] = relationship("Log", back_populates="file")


class Log(Base):
    __tablename__ = "log"

    id = Column(Integer, primary_key=True)
    user_id: int = Column(Integer, ForeignKey("user.id"), nullable=False)
    file_id: int = Column(Integer, ForeignKey("file.id"), nullable=False)
    remote_addr: str = Column(String(200), nullable=False)
    timestamp: datetime = Column(
        DateTime, nullable=False, default=datetime.now
    )

    user: "User" = relationship("User", back_populates="logs")
    file: "File" = relationship("File", back_populates="logs")
