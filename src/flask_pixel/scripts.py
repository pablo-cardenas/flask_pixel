import click
from flask import Flask
from flask.cli import with_appcontext

from .database import db_session
from .models import File
from .models import User


def add_user(email):
    db_session.add(User(email=email))
    db_session.commit()


@click.command("add-user")
@click.argument("email")
@with_appcontext
def add_user_command(email):
    """Add User to database"""
    add_user(email)
    click.echo(f"User {email=} added.")


def add_file(filename, mimetype):
    db_session.add(File(filename=filename, mimetype=mimetype))
    db_session.commit()


@click.command("add-file")
@click.argument("filename")
@click.argument("mimetype")
@with_appcontext
def add_file_command(filename, mimetype):
    """Add File to database"""
    add_file(filename, mimetype)
    click.echo(f"File {filename=} {mimetype=} added.")


def init_app(app: Flask):
    app.cli.add_command(add_user_command)
    app.cli.add_command(add_file_command)
